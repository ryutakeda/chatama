# CHATAMA

テキストをつぶやくとそれがARのタマシイになってゆらゆらと漂うiosアプリです。 スマホをかざして街中にあるみんなのタマシイを見てみましょう。
このアプリは言霊のイメージからプロダクトのコンセプトを広げ、各機能もタマシイを意識し た設計になっています。
また、LocationBasedARという技術を使うことによってタマシイは特定の場所に 紐づいて記録されます。この技術によってヴァーチャル上の空間でしか存在しないタマシイ は現実の場所に漂うのです。




### 概要



### How do I get set up?

メインとなるスクリプトだけ載せています。
 
/Asset以下にはSoulContollerとSoulLocationの2つのディレクトリがあります。


## SoulContoller



**SoulEdit.cs**  
  
ユーザーがテキストを入力し決定するとその内容をDBに書き込み、AR画面に遷移します。

**SoulGenerator.cs**  
  
DBからテキスト、位置情報、角度、を取得して変数に入れ、タマシイを作成。生成されたタマシイにはそれぞれ*DirectionAjduster.cs* *DistanceAjduster.cs*
がアタッチしておりこれらがそれぞれのタマシイの角度と位置を調整する。

**SoulTouch.cs**  
  
3D空間上のタマシイを2Dのスマホ画面からタッチできるようにするスクリプト。タッチイベントが発生したらEdit画面に遷移する。

## SoulLocation

**Compass.cs**  
  
現在の真北を取得します。

**DirectionAjduster.cs**  
  
Compass.csで取得した現在の北とDBに保存された過去の北の角度をもとにカメラからのタマシイの距離を調整します。

**DistanceAjduster.cs**  
  
DistanceCl.csで計算した距離をもとにタマシイとカメラの位置を調整します。

**DistanceCl.cs**  
  
ヒュベニの公式という数式をスクリプトに置き換えたものです。
2点間の緯度経度を入れると距離を返します。

**LocationUpdater.cs**  
  
緯度、経度、高度、精度情報を取得します。




