﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;


public class SoulEdit : MonoBehaviour {
	private SoulGenerator soulGene;
	private SoulTouch     soulTouch;
	GameObject locationGetter;
	private LocationUpdater updater;                  //位置情報
	private Compass compass;                          //コンパス
	private float horizontalAccuracy;
	private double prsntLat;
	private double prsntLng;
	private double prsntAlt;
	private double prsntN_Angl;                       //present   //compassから取得した現在の北からの差角

	private static	InputField inputField;
	private static   Text text;
	private static   string textValue;
	private static   string inputValue;
	private static   string inputedValue;
	private static   GameObject canvas;
	public static   string getInputValueFromEdit() {
		return inputValue;
	}
	private static bool penButtonFlg;
	public static   bool getPenButtonFlg() {
		return penButtonFlg;
	}
	private static bool instantiateFlg;
	public static bool getInstantiateFlg(){
		return instantiateFlg;
	}

	private static float      soulCount;               //Prefubの連番カウント
	private static string     soulNameB;
	public static string     getSoulNameB(){
		return soulNameB;
	}

	private  static string userId;
	private  int location = 2;                   // ダミーデータ
	private  string key ;
	private  string keyFromDB ;
	private  bool generateFlg;                   //提出用の制限フラグ

	private DatabaseReference _FirebaseDB;
	private DatabaseReference _FirebaseDB_text;


	// Use this for initialization
	void Start () {

		locationGetter     = GameObject.Find ("LocationGetter");
		updater            = locationGetter.GetComponent<LocationUpdater>();    //scriptを取得
		compass            = locationGetter.GetComponent<Compass>();            //scriptを取得

		horizontalAccuracy = updater.HorizontalAccuracy;
		prsntLat           = updater.PrsntLat;
		prsntLng           = updater.PrsntLng;
		prsntAlt           = updater.PrsntAlt;


		prsntN_Angl        = compass.PrsntN_Angl;
		generateFlg        = soulGene.GenerateFlg;
		keyFromDB          = soulGene.KeyFromDB;

		canvas             = GameObject.Find("Canvas");
		inputField         = GetComponent<InputField>();                   //        *********************************************************
		text               = canvas.GetComponentInChildren<Text>();        //        *ここでPlaceholderが取得されてしまっているが、Textを取得したい。*
                                                                           //        *********************************************************
		textValue          = SetName.getInputValue();
		penButtonFlg       = ButtonClick.getPenButtonFlg();                //       AR画面でsoulbuttonが押されたらtrueが入る
		instantiateFlg     = false;
		soulCount          = soulTouch.getSoulCount();
		soulNameB          = string.Format("Soul-Chat{0}",soulCount);



		if(textValue != null)

		{
			inputField.ActivateInputField();                 // フォーカス
			text.text  = textValue;                          //すでに入力されている場合はここで反映
			Debug.Log("既存テキストは" + text.text);

		}


		if(SignIn.getSignInedUser() != null ){
			userId = SignIn.getSignInedUser();
			Debug.Log(" from getSignInedUser  is " + userId);
		}


		if(SignUp.getSignUpedUser() != null){
			userId = SignUp.getSignUpedUser();
			Debug.Log(" from getSignUpedUser  is " + userId);
		}



		// Set up the Editor before calling into the realtime database.
		FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("hogehoge");
		// Get the root reference location of the database.
		DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;
		_FirebaseDB = FirebaseDatabase.DefaultInstance.GetReference("soulInfo/soulKey");
		_FirebaseDB_text = FirebaseDatabase.DefaultInstance.GetReference (string.Format ("soulInfo/soulKey/{0}", keyFromDB));






	}



	private void ReturnToAR (){
		SceneManager.LoadScene("SoulAR");
	}



	private void ChangeText (){                                   //       入力後、Enterを押すと呼び出される関数


		                                                         //       ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		                                                         //       :     SoulARに新しくタマシイobjectを追加するかの処理                                    ::::::
		                                                         //       ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


		switch(penButtonFlg){                                    //  タマシイボタンbool

		  case true:

			penButtonFlg   = false;
			inputValue     = inputField.text;
			setSoulData();                                       //新規ソウルデータをDBにセット
     	    break;


		  case false:

			Debug.Log("更新作業");
		   	inputValue = inputField.text;
		   	changeSoulData();                                     //既存のソウルデータのテキスト部分を変更
		   	break;
	  }


	}



	private void setSoulData(){


		key = "soul" + FirebaseDatabase.DefaultInstance.GetReference("soulInfo").Child("soulKey").Push().Key;


		Dictionary<string, object> itemMap = new Dictionary<string, object>();
		itemMap.Add("userId",      userId);
		itemMap.Add("text",        inputValue);
		itemMap.Add("location",  location);
		itemMap.Add("lat",      prsntLat);
		itemMap.Add("lng",      prsntLng);
		itemMap.Add("alt",      prsntAlt);
		itemMap.Add ("N_Angl", prsntN_Angl);
		itemMap.Add ("horizontalAccuracy", horizontalAccuracy);
		itemMap.Add("updatedate", System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));

		Dictionary<string, object> map = new Dictionary<string, object>();
		map.Add(key, itemMap);

		_FirebaseDB.UpdateChildrenAsync(map);
		ReturnToAR ();

	}


	private void changeSoulData(){

		//userIdをもとにkeyを並び替えて、userIdと一致するkeyを取得
		//取得後、そのkeyを元にmapを更新


		key = "soul" + FirebaseDatabase.DefaultInstance.GetReference("soulInfo").Child("soulKey").Push().Key;



		Dictionary<string, object> itemMap = new Dictionary<string, object>();
		itemMap.Add("userId",      userId);
		itemMap.Add("text",        inputValue);
		itemMap.Add("location",  location);
		itemMap.Add("lat",      prsntLat);
		itemMap.Add("lng",      prsntLng);
		itemMap.Add("alt",      prsntAlt);
	    itemMap.Add ("N_Angl", prsntN_Angl);
		itemMap.Add ("horizontalAccuracy", horizontalAccuracy);
		itemMap.Add("updatedate", System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));

		Dictionary<string, object> map = new Dictionary<string, object>();
		map.Add(keyFromDB, itemMap);

	_FirebaseDB.UpdateChildrenAsync(map);
		ReturnToAR ();

	}




} // class end
