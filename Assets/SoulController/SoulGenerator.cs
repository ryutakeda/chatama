﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using UnityEngine.UI;
using UnityEngine.SceneManagement;

using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;




public class SoulGenerator : MonoBehaviour {



	private   bool generateFlg;                   //提出用の制限フラグ
//	private static  bool getGenerateFlg(){
//		return generateFlg;
//	}
	public bool GenerateFlg{

		get{ return this.generateFlg; }

	}

	private  static string userId;
	private  string textFromDB;
	public   string TextFromDB{

		get{ return this.textFromDB; }

	}
//	private static string getTextFromDB(){
//		return textFromDB;
//	}

	private   string keyFromDB;
	public    string KeyFromDB{

		get{ return this.keyFromDB; }

	}
//	private static string getKeyFromDB(){
//		return keyFromDB;
//	}

	private double pastN_Angl;        //過去の北
	public double PastN_Angl{

		get{ return this.pastN_Angl; }  //取得用

	}
	private double pastLat;           //過去の緯度
	public double PastLat{

		get{ return this.pastLat; }  //取得用

	}
	private double pastLng;           //過去の経度
	public double PastLng{

		get{ return this.pastLng; }  //取得用

	}
	private double pastAlt;

	private  int location;
	private  string key ;
	private DatabaseReference _FirebaseDB;

	private GameObject soulGizmo;    //タマシイ生成用


	void Start () {


		if(SignIn.getSignInedUser() != null ){

			userId = SignIn.getSignInedUser();

		}

		if(SignUp.getSignUpedUser() != null){

			userId = SignUp.getSignUpedUser();

		}


		Debug.Log(" userId is " + userId);
		// Set up the Editor before calling into the realtime database.
		FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("hogehoge");
		// Get the root reference location of the database.
		DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;
		_FirebaseDB = FirebaseDatabase.DefaultInstance.GetReference("soulInfo/soulKey");
		GetSoulData();


	}

	// Update is called once per frame
	void Update () {

	}

	void GetSoulData() {

		Debug.Log ("from GetSoulData");

		// userIdと等しいものを取得
		_FirebaseDB.OrderByChild("userId").EqualTo(userId).GetValueAsync().ContinueWith(task => {

			if (task.IsFaulted) {
				// 取得エラー
				Debug.Log("[ERROR] get ranking sort");
			} else if (task.IsCompleted) {
				// 取得成功
				Debug.Log("[INFO] get soul success.");

				DataSnapshot snapshot = task.Result;
				IEnumerator<DataSnapshot> result = snapshot.Children.GetEnumerator();


				string json = snapshot.GetRawJsonValue();

				DataSnapshot item = JsonUtility.FromJson<DataSnapshot>(json);

				while (result.MoveNext()) {


					DataSnapshot data = result.Current;

					textFromDB        =   (string)data.Child("text").Value;
					keyFromDB         =   (string)data.Key ;
					location          =   (int)(long)data.Child("location").Value;
       				pastLat           =   (double)data.Child("lat").Value;
					pastLng           =   (double)data.Child("lng").Value;
					pastAlt           =   (double)data.Child("alt").Value;
					pastN_Angl        =   (double)data.Child("N_Angl").Value;


					Instantiate (soulGizmo); //タマシイ生成
					Debug.Log("soulGixzmo !!!");

				}

			}
		});
	}




}
