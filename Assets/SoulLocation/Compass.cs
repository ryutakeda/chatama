﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Compass : MonoBehaviour {


	private double prsntN_Angl;

	public double PrsntN_Angl{

		get{ return this.prsntN_Angl; }  //取得用

	}
	private float headingAccuracy;

	public float HeadingAccuracy{

		get{ return this.headingAccuracy; }  //取得用

	}


	void Start () {

		Input.compass.enabled = true;
		Input.location.Start ();

		Debug.Log (string.Format("<b>精度</b>：{0}", Input.compass.headingAccuracy));
		Debug.Log (string.Format("<b>タイムスタンプ</b>：{0}", Input.compass.timestamp));
		Debug.Log ("trueHeading is " + Input.compass.trueHeading);

		this.prsntN_Angl = Input.compass.trueHeading;

		
	}
	

	void Update () {

		this.prsntN_Angl = Input.compass.trueHeading;
		this.headingAccuracy = Input.compass.headingAccuracy;

	}


}
