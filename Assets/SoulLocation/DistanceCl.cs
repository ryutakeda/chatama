﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceCl : MonoBehaviour {

	GameObject soulController;
	GameObject locationGetter;
	private LocationUpdater updater;                                          //位置情報
	private SoulGenerator   soulGetter;
	private float  alt;

	//	private double  grsEighy_a = 6377397.155d;
	//	private double  grsEighy_b = 6356079.000000d;
	//	private double  grsEighy_aspectRatioRP = 299.152813000d;              //世界測地系定数
	//	private double  grsEighy_eccentricity2 = 0.00667436061028297d;
	//	private double  grsEighy_numerOfM = 6334832.10663254d;

	private double wgs84_a             = 6378137.000d ;
	private double wgs84_b             = 6356752.314245d;
	private double wgs84_aspectRatioRP = 298.257223563 ;                      //GPS 定数
	private double wgs84_eccentricity2 = 0.00669437999019758 ;
	private double wgs84_numerOfM      = 6335439.32729246;

	private double latA;
	private double lngA;
	private double prsntLat;
	private double prsntLng;

	private double horizontalAccuracy;
	private double verticalAccuracy;


	private double latB;
	private double lngB;
	private double pastLat;
	private double pastLng;

	private double dOfLat;
	private double dOfLng;
	private double sumOfLat;

	private double avrOfLat;

	private double merediOfRadi;
	private double merediOfPrmVrtcl;
	private double innerW;
	private double w;
	private double m;
	private double n;
	private double my;
	private double sin;

	private double dy;
	private double dx;
	private double dym;
	private double dxncos;

	private double innerHubeny;

	public double distance;

	private double deg2rad(double deg){                //deg2radは度をラジアンに変更する関数
		return deg * Math.PI / 180.0d;       
	}


	// Use this for initialization
	void Start () {



		Debug.Log ("distanceCl is called !!");


		locationGetter = GameObject.Find ("LocationGetter");
		updater        = locationGetter.GetComponent<LocationUpdater>(); //scriptを取得

		prsntLat = updater.PrsntLat;
		prsntLng = updater.PrsntLng;

		horizontalAccuracy = updater.HorizontalAccuracy;
		verticalAccuracy   = updater.VerticalAccuracy;

	
		soulController = GameObject.Find ("SoulController");
		soulGetter     = soulController.GetComponent<SoulGenerator>();
		pastLat        = soulGetter.PastLat ;
		pastLng        = soulGetter.PastLng ;

		latA = prsntLat;           // 現在の緯度経度
		lngA = prsntLng;

		latB = pastLat;            // 過去の緯度経度
		lngB = pastLng; 	

//		latA = 35.65500d;            //Tokyo
//		lngA = 139.74472d;
//
//		latB = 36.10056d;           //Tsukuba
//		lngB = 140.09111d; 	


//		latA = 35.548383d;          //BedRoom
//		lngA = 139.638704d;

	
//		latB = 35.548360d;          //Kitchin
//		lngB = 139.638691d; 	

//		prsntLat = 35.548500d;
//		prsntLng = 139.63861d;      //ダミー計算用
//
//		pastLat  = 35.548362d;
//		pastLng  = 139.63874d;

//		prsntLat = 35.548403d;
//		prsntLng = 139.638803d;      //ダミー計算用2
//	
//    	pastLat  = 35.548396d;
//		pastLng  = 139.638801d;

		dOfLat   = latA - latB;
		dOfLng   = lngA - lngB;
		sumOfLat = latA + latB;

	    my  = deg2rad((sumOfLat) / 2.0d);
		dy  = deg2rad(dOfLat);
		dx  = deg2rad(dOfLng);	
		sin = Math.Sin(my);               // need to cast


		innerW = 1.0d -  wgs84_eccentricity2  * sin * sin;   //変更した
		w      = Math.Sqrt (innerW);

		avrOfLat = sumOfLat / 2.0d;
		m        =    wgs84_numerOfM / (w * w * w);
		n        =  wgs84_a / w;


		distance = calcDistHubeny();
	
	}



	public  double calcDistHubeny(){                        //ヒュベニの公式計算メソッド
		dym    = dy * m;
		dxncos = dx * n * Math.Cos(my);
		innerHubeny = dym * dym + dxncos * dxncos;
		return Math.Sqrt(innerHubeny);
		Debug.Log ("culculation is succses!!");

	}


}
