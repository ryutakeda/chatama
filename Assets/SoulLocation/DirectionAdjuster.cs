﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//   このクラスはsoulgizmoにアタッチして使う
//   gizmo回転用

public class DirectionAdjuster : MonoBehaviour {

	private Vector3 axis ;
	GameObject locationGetter;
	GameObject soulController;
	private Compass compass;
	private SoulGenerator soulGene;
	private float prsntN_Angl;   //present   //compassから取得した現在の北からの差角
	private float pastN_Angl;                //DBから取得した記録された北からの差角
	private float prsntSoul_Angl;
	private Quaternion q;
	private  Vector3 cubePos;

	// Use this for initialization
	void Start () {

		locationGetter  =   GameObject.Find ("LocationGetter");
		soulController  =   GameObject.Find ("SoulController");
		compass         =   locationGetter.GetComponent<Compass>();  //scriptを取得
		soulGene        =   soulController.GetComponent<SoulGenerator>(); 
		prsntN_Angl     =   (float)compass.PrsntN_Angl;
		pastN_Angl      =   (float)soulGene.PastN_Angl;
	    prsntSoul_Angl  =   -prsntN_Angl + pastN_Angl;


		this.transform.rotation = Quaternion.Euler(0.0f, prsntSoul_Angl, 0.0f); //現在の北の方向にタマシイを回転させる

	}
	

}
