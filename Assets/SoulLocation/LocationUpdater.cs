﻿using System.Collections;
using UnityEngine;
using System.Collections;

public class LocationUpdater : MonoBehaviour



{
	private float IntervalSeconds = 2.0f;
	private LocationServiceStatus Status;
	private LocationInfo Location;
	private double prsntLat;
	public double PrsntLat{

		get{ return this.prsntLat; }  

	}
	private double prsntLng;
	public double PrsntLng{

		get{ return this.prsntLng; }  

	}
	private double  prsntAlt;
	public double PrsntAlt{

		get{ return this.prsntAlt; }  

	}

	private float horizontalAccuracy;
	public float HorizontalAccuracy{

		get{ return this.horizontalAccuracy; }  

	}

	private float verticalAccuracy;
	public double VerticalAccuracy{

		get{ return this.verticalAccuracy; }  

	}



	IEnumerator Start()

	{
		while (true)
		
		{
			this.Status = Input.location.status;
		
			if (Input.location.isEnabledByUser)
			
			{
				switch(this.Status)

				{


				case LocationServiceStatus.Stopped:
						
						Input.location.Start (5f);
						break;

				case LocationServiceStatus.Running:

						this.horizontalAccuracy = Location.horizontalAccuracy;
						this.verticalAccuracy   = Location.verticalAccuracy;
						

					if(horizontalAccuracy <= 30f){
						Debug.Log ("Good_horizontalAccuracy is " + horizontalAccuracy);
						this.Location = Input.location.lastData;
						this.prsntLat = Location.latitude;
						this.prsntLng = Location.longitude;
						this.prsntAlt = Location.altitude;
					}



					break;
				default:
					break;
				}
			}
			else
			{
				
				Debug.Log("位置情報を有効にしてください");
			}

			// 指定した秒数後に再度判定を走らせる
			yield return new WaitForSeconds(IntervalSeconds);
		}
	}




}